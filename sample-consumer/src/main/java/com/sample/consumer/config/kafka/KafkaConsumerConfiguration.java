package com.sample.consumer.config.kafka;

import com.sample.consumer.event.KafkaEvent;
import com.sample.consumer.event.SamplePayload;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import com.fasterxml.jackson.core.type.TypeReference;

import java.util.HashMap;
import java.util.Map;
@RequiredArgsConstructor
@Configuration
public class KafkaConsumerConfiguration {

    private final KafkaProperties kafkaProperties;

    @Value("${kafka.topic.sample.payload}")
    @Getter
    private String samplePayloadTopic;


    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, KafkaEvent<SamplePayload>>
    kafkaListenerContainerFactory() {
        ConcurrentKafkaListenerContainerFactory<String, KafkaEvent<SamplePayload>> factory =
                new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(consumerFactory());
        return factory;
    }


    @Bean
    public ConsumerFactory<String, KafkaEvent<SamplePayload>> consumerFactory() {
        JsonDeserializer<KafkaEvent<SamplePayload>> deserializer =
                new JsonDeserializer<>(new TypeReference<KafkaEvent<SamplePayload>>() {});
        deserializer.setRemoveTypeHeaders(false);
        deserializer.addTrustedPackages("com.sample.publisher.kafka");
        deserializer.setUseTypeMapperForKey(true);
        return new DefaultKafkaConsumerFactory(consumerConfigs(), new StringDeserializer(),
                deserializer);
    }

    @Bean
    public Map<String, Object> consumerConfigs() {
        Map<String, Object> props = new HashMap<>();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaProperties.getBootstrapServers());
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, JsonDeserializer.class);
        props.put(ConsumerConfig.GROUP_ID_CONFIG, kafkaProperties.getConsumerGroup());
        return props;
    }
}
