package com.sample.publisher;

import com.sample.publisher.data.KafkaEvent;
import com.sample.publisher.data.SamplePayload;
import com.sample.publisher.kafka.publisher.EventPublisher;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PayloadService {

    private final EventPublisher eventPublisher;

    public void processPayload(@NonNull KafkaEvent<SamplePayload> event) {
        eventPublisher.publishSamplePayload(event);
    }
}
