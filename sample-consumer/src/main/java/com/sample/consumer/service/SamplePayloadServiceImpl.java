package com.sample.consumer.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sample.consumer.ConversionException;
import com.sample.consumer.event.SamplePayload;
import com.sample.consumer.repository.PayloadRepository;
import com.sample.consumer.repository.SamplePayloadEntity;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.postgresql.util.PGobject;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor
public class SamplePayloadServiceImpl implements SamplePayloadService {

    private static final String JSONB = "jsonb";

    private final AnonymizationService anonymizationService;

    private final ObjectMapper objectMapper;

    private final PayloadRepository payloadRepository;


    @Override
    public void save(SamplePayload payload) {
        // Convert payload to json
        final SamplePayload anonymizedPayload = anonymizationService.anonymize(payload);
        // Convert to entity.
        final SamplePayloadEntity samplePayloadEntity = createSamplePayloadEntity(anonymizedPayload);
        // Persist.
        payloadRepository.save(samplePayloadEntity);
    }

    private SamplePayloadEntity createSamplePayloadEntity(SamplePayload anonymizedPayload) {
        final PGobject pGobject = convertToPayloadJson(anonymizedPayload);
        return SamplePayloadEntity.builder()
                .id(UUID.randomUUID().toString())
                .creationTime(LocalDateTime.now())
                .eventId(anonymizedPayload.getEventId())
                .businessId(anonymizedPayload.getBusinessId())
                .payload(pGobject)
                .build();
    }

    private PGobject convertToPayloadJson(SamplePayload samplePayload) {
        PGobject jsonObject = null;
        try {
            String payloadAsString = objectMapper.writeValueAsString(samplePayload);
            jsonObject = new PGobject();
            jsonObject.setType(JSONB);
            jsonObject.setValue(payloadAsString);
        } catch (SQLException | JsonProcessingException e) {
            final String message = "Can't persist the payload due to conversion issues!";
            log.error(message);
            throw new ConversionException(message, e);
        }
        return jsonObject;
    }
}
