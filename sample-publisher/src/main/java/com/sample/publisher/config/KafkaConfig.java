package com.sample.publisher.config;

import com.sample.publisher.data.KafkaEvent;
import com.sample.publisher.data.SamplePayload;
import lombok.Data;
import lombok.Getter;
import org.apache.kafka.clients.admin.AdminClientConfig;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaAdmin;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.support.serializer.JsonSerializer;

import java.util.HashMap;
import java.util.Map;

@Configuration
@Getter
public class KafkaConfig {

    @Autowired
    private KafkaProperties kafkaProperties;

    // topic name read from properties.
    @Value("${kafka.topic.sample.payload}")
    private String samplePayloadTopic;

    @Bean
    public KafkaAdmin kafkaAdmin() {
        Map<String, Object> configs = new HashMap<>();
        configs.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaProperties.getBootstrapServers());
        return new KafkaAdmin(configs);
    }

    @Bean
    public NewTopic createSamplePayloadTopic() {
        return new NewTopic(
                samplePayloadTopic,  // The name of the topic (plural).
                kafkaProperties.getNumberOfPartitions(),           // Number of partitions
                kafkaProperties.getReplicationFactor()     // Replication Factor
        );
    }


    @Bean
    // TODO migrate to Protobuf or Avro in production instead of Json.
    public <T> ProducerFactory<String, T> jsonProducerFactory() {
        Map<String, Object> configProps = new HashMap<>();
        configProps.put(
                ProducerConfig.BOOTSTRAP_SERVERS_CONFIG,
                kafkaProperties.getBootstrapServers());
        configProps.put(
                ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,
                StringSerializer.class);
        configProps.put(
                ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,
                JsonSerializer.class);
        return new DefaultKafkaProducerFactory<>(configProps);
    }

    @Bean
    public KafkaTemplate<String, KafkaEvent<SamplePayload>> kafkaEventKafkaTemplate() {
        return new KafkaTemplate<>(this.jsonProducerFactory());
    }
}
