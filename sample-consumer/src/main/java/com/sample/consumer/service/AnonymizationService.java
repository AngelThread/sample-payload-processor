package com.sample.consumer.service;

import com.sample.consumer.event.SamplePayload;
import lombok.NonNull;

/**
 * Responsible of anonymization of the payload.
 */
public interface AnonymizationService {

    /**
     * Responsible of creating a new value object by anonymizing fields immutable way.
     *
     * @param payload value object to anonymize.
     * @return
     */
    SamplePayload anonymize(@NonNull SamplePayload payload);
}
