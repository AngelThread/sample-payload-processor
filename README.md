This project has 2 modules which are separate Spring-Boot application, named sample-publisher and sample-consumer. Sample-publisher accepts data via HTTP Post /payloads
JSON endpoint and transmits the received data which will be referred as payload/sample payload from now on to Kafka. The sample-consumer application read the payload
from Kafka and anonymize it before putting to PostgreSQL database under sample_payloads table which has the whole payload
as Jsonb column and metadata information (event_id, timestamp) as separate columns in addition to indexed business_id column.

To have modules up and running, please run run_me.sh as following. This command will build the modules an will create 
docker images which later will be used in the docker-compose file in the file. With this command, zookeeper, kafka, publisher,
consumer and finally KSQL's server,client will be running.

There are HELP.md files under related modules.

## To start the system
Please run following command, it will build the 3 maven projects and it will create
2 different images per each application, and finally will trigger docker-compose.

- sh ./run_me.sh


Please see following system diagram for the bigger picture of the system.

![Alt text](system_diagram.png?raw=true "System diagram")


## Sample Publisher

Sample publisher is a Java application which has an API that accepts sample payload information.

 - API has POST /payloads endpoint which accepts the sample payload in the challenge description.
 - SamplePayload is published on Kafka partitioning by business id.
 - SamplePayload is being published as Json to Kafka, but in production I would use Protobuf or Avro
 to have better backward/forward compatibility. 
- For integration tests embedded Kafka and embedded PostgreSQL database used.


This application is built using with following technologies.
  - Maven
  - Spring Boot
  - Lombok
  - Spring-Kafka
  - Rest
  - MapStruct
  - KSQL
 * Please enable annotation processor for lombok after installing Lombok plugin for Intellij. 
## Sample Consumer

Application is powered by Spring Boot Kafka and Spring Boot JDBC.

- As database PostgreSQL database used with Liquibase migration tool in place, and the event read from Kafka topic saved as Json document in sample_payloads table with 
metadate information.

- For integration tests embedded Kafka and embedded PostgreSQL database used.

To be able to connect to database, after running run_me file with as bellow.

- Host:localhost
- User:consumer
- Port:5432
- Password:password
- Database:consumer

- sample_payloads table has following columns and 
    - id, column with generated id to provide uniquness of events 
    - creation time, time of the event persisted
    - business_id, indexed, (decided to ne indexed since it is natural key)
    - payload, received event as json document

Some useful queries to see anonymized data.
- SELECT count(*) FROM sample_payloads;
- SELECT * FROM sample_payloads WHERE business_id='1SWheh84yJXfytovILXOAQ';
- SELECT * FROM sample_payloads WHERE payload @> '{"address":"2818 E Camino Acequia Drive"}';
- SELECT * FROM sample_payloads WHERE payload->>'address' IS NOT NULL;
- SELECT *  FROM sample_payloads  WHERE payload @> '{"attributes":{"GoodForKids": "False"}}';
- SELECT * FROM sample_payloads WHERE payload->'hours'->>'Tuesday' IS NOT null;

### To see the raw data

 1) To see the data as it on Kafka broker, please run following command after running run_me.sh
    - kafka-console-consumer --bootstrap-server localhost:9092 --topic SAMPLE_PAYLOADS
    Note: To add meta-date to payload received from API, there is a generic KafkaEvent class carries data received with
    eventId and timestamp fields. The real data can be read in the field call payload.

 2) Querying data by KSQL please run following command in a different terminal than option 1.
 // Connect to KSQL (this is a comment)  
    > ksql
    > SET 'auto.offset.reset'='earliest';
    // Create a new stream. (this is a comment)                                                                                                                  
    > CREATE STREAM sample_payloads (payload VARCHAR) WITH (KAFKA_TOPIC='SAMPLE_PAYLOADS', VALUE_FORMAT='JSON');                                                                                             
    > select payload from sample_payloads;
    // Create new stream with selected fields. (this is a comment)                                                                                            
    >  CREATE STREAM flattened_payload
            (Payload STRUCT<
          business_id VARCHAR,
                  Name VARCHAR, 
                  address VARCHAR, 
                  city VARCHAR, 
             state VARCHAR, 
             latitude VARCHAR,
      	longitude VARCHAR,
      	stars VARCHAR>
       )WITH (KAFKA_TOPIC='SAMPLE_PAYLOADS', VALUE_FORMAT='JSON');
    // select new stream (this is a comment)                                                                                                 
    > select PAYLOAD from flattened_payload;                                                                                                                                                                                          


