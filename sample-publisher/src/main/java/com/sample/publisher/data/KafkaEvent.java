package com.sample.publisher.data;

import lombok.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.UUID;

/**
 * Represents a generic Kafka event with information about event besides payload.
 */
@Data
@NoArgsConstructor(force = true)
@AllArgsConstructor
@Builder
public class KafkaEvent<T>  implements Serializable {
    /**
     * Provides uniques for the event.
     * Event id as UUID.randomUUID().toString().
     */
    @NonNull
    private String eventId;
    /**
     * Even't creation time as timestamp.
     * The number of seconds from the Java epoch of 1970-01-01T00:00:00Z.
     */
    @NonNull
    private long timestamp;

    // Payload can not be null!
    @NonNull
    private T payload;

    public static <T> KafkaEvent<T> createKafkaEvent(T payload) {

        return KafkaEvent.<T>builder().payload(payload)
                .eventId(UUID.randomUUID().toString())
                .timestamp(Instant.now().getEpochSecond())
                .build();
    }

}
