package com.sample.consumer.repository;

import lombok.NonNull;

/**
 * Repository as bridge between datasource and application related to persistence operations of
 * {@link com.sample.consumer.event.SamplePayload}
 */
public interface PayloadRepository {

    void save(@NonNull SamplePayloadEntity samplePayloadEntity);
}
