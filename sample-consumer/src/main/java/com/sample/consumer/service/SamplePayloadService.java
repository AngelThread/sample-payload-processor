package com.sample.consumer.service;

import com.sample.consumer.event.SamplePayload;
import org.springframework.stereotype.Service;

/**
 * Service responsible of payload operations.
 */
@Service
public interface SamplePayloadService {

    void save(SamplePayload payload);
}
