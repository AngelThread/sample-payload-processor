package com.sample.consumer;

import com.sample.consumer.event.KafkaEvent;
import com.sample.consumer.event.SamplePayload;
import com.sample.consumer.service.SamplePayloadService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.kafka.annotation.KafkaHandler;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Slf4j
@Component
@KafkaListener(
        topics = {
                "#{kafkaConsumerConfiguration.getSamplePayloadTopic()}"
        },
        groupId = "${kafka.consumer.group}",
        containerFactory = "kafkaListenerContainerFactory")
@RequiredArgsConstructor
@ConditionalOnProperty(value = "kafka.enabled", havingValue = "true")
public class KafkaEventConsumer {

    private final SamplePayloadService service;

    @KafkaHandler
    public void listenKafkaEvents(KafkaEvent<SamplePayload> event) {
        try {
            if (Objects.isNull(event) || Objects.isNull(event.getPayload())) {
                log.error("Not acceptable message received!");
                return;
            }
            log.debug("Test consumer received new payload with id:{}", event.getEventId());
            service.save(event.getPayload());
        } catch (ConversionException conversionException) {
            // This means that there is problem with converting the message read from Kafka to entity so this message will
            // be skipped.
            // TODO send this kind of messages to dead letter queue implementation
            log.error("Conversion problem!", conversionException);
            return;
        } catch (Exception ex) {
            // Any other exception is going to be skipped, this may cause losing some Kafka messages either because of
            // a bug or a fualty message.
            // TODO distinguish which exceptions needs to be retired and don't catch them
            // TODO implement a dead letter queue for faulty messages and send them to another Kafka topic
            log.error("For now do nothing, skip the faulty message!", ex);
            return;
        }

    }

    /**
     * Default handler for not expected message types,
     * or this method will be called in case of a deserialization problem.
     *
     * @param object
     */
    @KafkaHandler(isDefault = true)
    public void defaultHandler(Object object) {
        log.error("Unexpected messaged received!Event:{}", object);
    }
}
