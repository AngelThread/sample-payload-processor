package com.sample.publisher.mapper;

import com.sample.publisher.data.SamplePayload;
import com.sample.publisher.dto.SamplePayloadDTO;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface SamplePayloadMapper {

    SamplePayload toModel(SamplePayloadDTO samplePayloadDTO);
}
