#!/usr/bin/env bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

./mvnw clean install

docker-compose -f $DIR/docker-compose.yml up
