package com.sample.consumer.repository;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@RequiredArgsConstructor
@Slf4j
public class PayloadRepositoryImpl implements PayloadRepository {

    public static final String INSERT_STATEMENT =
            "insert into sample_payloads (id, creation_time, event_id, business_id, payload) values(?,?,?,?,?)";
    private final JdbcTemplate jdbcTemplate;

    private final ObjectMapper objectMapper;

    @Override
    @Transactional
    public void save(@NonNull SamplePayloadEntity samplePayloadEntity) {
        // Persist
        jdbcTemplate.update(
                INSERT_STATEMENT,
                samplePayloadEntity.getId(),
                samplePayloadEntity.getCreationTime(),
                samplePayloadEntity.getEventId(),
                samplePayloadEntity.getBusinessId(),
                samplePayloadEntity.getPayload());
    }
}
