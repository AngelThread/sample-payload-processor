package com.sample.consumer.service;

import com.sample.consumer.event.SamplePayload;
import lombok.NonNull;
import org.springframework.stereotype.Service;

@Service
public class AnonymizationServiceImpl implements AnonymizationService {
    // TODO read from application properties
    private static final String REMOVED = "REMOVED";

    /**
     * // TODO read fields to anonymize from application properties
     * Uses hard coded logic to anonymize fields
     *
     * @param payload value object to anonymize.
     * @return
     */
    @Override
    public SamplePayload anonymize(@NonNull SamplePayload payload) {
        return payload.toBuilder()
                .name(REMOVED)
                .address(REMOVED)
                .latitude(REMOVED)
                .longitude(REMOVED)
                .build();
    }
}
