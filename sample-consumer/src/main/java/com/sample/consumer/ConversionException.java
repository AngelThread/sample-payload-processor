package com.sample.consumer;

/**
 * Thrown when there is format conversion problem.
 */
public class ConversionException extends RuntimeException {
    public ConversionException(String message, Throwable t) {
        super(message, t);
    }
}
