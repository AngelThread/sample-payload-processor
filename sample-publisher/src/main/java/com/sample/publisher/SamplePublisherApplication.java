package com.sample.publisher;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SamplePublisherApplication {

	public static void main(String[] args) {
		SpringApplication.run(SamplePublisherApplication.class, args);
	}

}
