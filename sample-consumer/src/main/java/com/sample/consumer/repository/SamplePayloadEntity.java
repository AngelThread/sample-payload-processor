package com.sample.consumer.repository;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.Value;
import org.postgresql.util.PGobject;

import java.time.LocalDateTime;

/**
 * Payload representation corresponds to data on data source for an even received from messaging layer.
 */
@Value
@Builder
@NoArgsConstructor(force = true)
@AllArgsConstructor
public class SamplePayloadEntity {

    private String id;

    private LocalDateTime creationTime;

    private String eventId;

    private String businessId;

    private PGobject payload;
}
