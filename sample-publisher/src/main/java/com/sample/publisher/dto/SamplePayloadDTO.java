package com.sample.publisher.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

/**
 * DTO object which is being used to accept sample payload.
 * JsonProperty  {@link com.fasterxml.jackson.annotation.JsonProperty} annotation in the class is being used to map json fields
 * which has a different naming than java fields to java fields.
 * Primitive boxed types Long, Integer being used to be able to accept null.
 */

@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class SamplePayloadDTO {

    @JsonProperty("business_id")
    private String businessId;

    private String name;

    private String address;

    private String city;

    private String state;

    @JsonProperty("postal_code")
    private String postalCode;

    private Double latitude;

    private Double longitude;

    private Double stars;

    @JsonProperty("review_count")
    private Long reviewCount;

    @JsonProperty("is_open")
    private Integer isOpen;

    private Map<String, String> attributes;

    private String categories;

    private Map<String, String> hours;

}
