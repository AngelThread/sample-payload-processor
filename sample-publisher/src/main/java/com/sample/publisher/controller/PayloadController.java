package com.sample.publisher.controller;

import com.sample.publisher.PayloadService;
import com.sample.publisher.data.KafkaEvent;
import com.sample.publisher.data.SamplePayload;
import com.sample.publisher.dto.SamplePayloadDTO;
import com.sample.publisher.mapper.SamplePayloadMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * API for sample payload operations.
 */
@RestController
@RequestMapping(value = "/payloads", consumes = MediaType.APPLICATION_JSON_VALUE)
@Slf4j
@RequiredArgsConstructor
public class PayloadController {
    private final PayloadService payloadService;
    private final SamplePayloadMapper samplePayloadMapper;

    @PostMapping
    public SamplePayloadDTO createPayload(@RequestBody SamplePayloadDTO samplePayloadDTO) {
        log.debug("Request received!");
        final SamplePayload payload = samplePayloadMapper.toModel(samplePayloadDTO);
        final KafkaEvent<SamplePayload> event = KafkaEvent.createKafkaEvent(payload);
        payloadService.processPayload(event);
        log.debug("Request processed!");
        return samplePayloadDTO;
    }
}
