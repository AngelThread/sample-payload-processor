package com.sample.publisher.mapper;

import com.sample.publisher.data.SamplePayload;
import com.sample.publisher.data.SamplePayload.SamplePayloadBuilder;
import com.sample.publisher.dto.SamplePayloadDTO;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2020-02-24T23:30:27+0100",
    comments = "version: 1.3.0.Final, compiler: javac, environment: Java 13.0.2 (Oracle Corporation)"
)
@Component
public class SamplePayloadMapperImpl implements SamplePayloadMapper {

    @Override
    public SamplePayload toModel(SamplePayloadDTO samplePayloadDTO) {
        if ( samplePayloadDTO == null ) {
            return null;
        }

        SamplePayloadBuilder samplePayload = SamplePayload.builder();

        samplePayload.businessId( samplePayloadDTO.getBusinessId() );
        samplePayload.name( samplePayloadDTO.getName() );
        samplePayload.address( samplePayloadDTO.getAddress() );
        samplePayload.city( samplePayloadDTO.getCity() );
        samplePayload.state( samplePayloadDTO.getState() );
        samplePayload.postalCode( samplePayloadDTO.getPostalCode() );
        if ( samplePayloadDTO.getLatitude() != null ) {
            samplePayload.latitude( String.valueOf( samplePayloadDTO.getLatitude() ) );
        }
        if ( samplePayloadDTO.getLongitude() != null ) {
            samplePayload.longitude( String.valueOf( samplePayloadDTO.getLongitude() ) );
        }
        samplePayload.stars( samplePayloadDTO.getStars() );
        samplePayload.reviewCount( samplePayloadDTO.getReviewCount() );
        samplePayload.isOpen( samplePayloadDTO.getIsOpen() );
        Map<String, String> map = samplePayloadDTO.getAttributes();
        if ( map != null ) {
            samplePayload.attributes( new HashMap<String, String>( map ) );
        }
        samplePayload.categories( samplePayloadDTO.getCategories() );
        Map<String, String> map1 = samplePayloadDTO.getHours();
        if ( map1 != null ) {
            samplePayload.hours( new HashMap<String, String>( map1 ) );
        }

        return samplePayload.build();
    }
}
