package com.sample.publisher.kafka.publisher;

import com.sample.publisher.config.KafkaConfig;
import com.sample.publisher.data.KafkaEvent;
import com.sample.publisher.data.SamplePayload;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.helpers.MessageFormatter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

import java.util.Optional;
import java.util.UUID;

@RequiredArgsConstructor
@Slf4j
@Component
@ConditionalOnProperty(value = "kafka.enabled", havingValue = "true")
public class EventPublisher {
    // kafka template.
    private final KafkaTemplate<String, KafkaEvent<SamplePayload>> samplePayloadKafkaTemplate;

    private final KafkaConfig kafkaConfig;

    public void publishSamplePayload(@NonNull KafkaEvent<SamplePayload> event) {

        log.debug("Publishing event with id:{}", event.getEventId());
        final String samplePayloadTopic = kafkaConfig.getSamplePayloadTopic();
        final SamplePayload payload = event.getPayload();
        // Using business id as Kafka topic partition key.
        final String businessId = payload.getBusinessId();

        ListenableFuture<SendResult<String, KafkaEvent<SamplePayload>>> listenableFuture =
                samplePayloadKafkaTemplate.send(samplePayloadTopic,
                        Optional.ofNullable(businessId).orElse(UUID.randomUUID().toString()), event);
        listenableFuture.addCallback(new ListenableFutureCallbackLogger<>(samplePayloadTopic));
    }


    private class ListenableFutureCallbackLogger<T> implements ListenableFutureCallback<SendResult<String, T>> {
        private String topic;

        ListenableFutureCallbackLogger(@NonNull final String topic) {
            this.topic = topic;
        }

        @Override
        public void onFailure(Throwable ex) {
            final String message = MessageFormatter
                    .format("process=send_message, status=failed, topic='{}'", topic)
                    .getMessage();
            log.error(message, ex);
        }

        @Override
        public void onSuccess(@Nullable SendResult<String, T> result) {
            log.debug("process=send_message, status=success, topic='{}', partition='{}', key='{}', offset='{}'",
                    result.getProducerRecord().topic(),
                    result.getProducerRecord().partition(),
                    result.getProducerRecord().key(),
                    result.getRecordMetadata().offset());
        }
    }

}
