package com.sample.publisher.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.Value;

import java.util.Map;

/**
 * Value object which decouples {@link com.sample.publisher.dto.SamplePayloadDTO} from the internal of the system,
 * by replacing it in the rest of the system.
 */

@Value
@Builder
@NoArgsConstructor(force = true)
@AllArgsConstructor
public class SamplePayload {

    @JsonProperty("business_id")
    private String businessId;

    private String name;

    private String address;

    private String city;

    private String state;

    @JsonProperty("postal_code")
    private String postalCode;

    // Kept as string to convert to "REMOVED" easily.
    private String latitude;

    // Kept as string to convert to "REMOVED" easily.
    private String longitude;

    private Double stars;

    @JsonProperty("review_count")
    private Long reviewCount;

    @JsonProperty("is_open")
    private Integer isOpen;

    private Map<String, String> attributes;

    private String categories;

    private Map<String, String> hours;

}
